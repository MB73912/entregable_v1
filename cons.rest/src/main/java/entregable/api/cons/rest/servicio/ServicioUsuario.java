package entregable.api.cons.rest.servicio;

import entregable.api.cons.rest.modelo.Usuario;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ServicioUsuario {
    private List<Usuario> arregloU = new ArrayList<Usuario>();

    public ServicioUsuario(){
        arregloU.add(new Usuario(1,"María Ramos","Centro"));
        arregloU.add(new Usuario(2,"Elena Santos","Norte"));
        arregloU.add(new Usuario(3,"Clara Jiménez","Sur"));
        arregloU.add(new Usuario(4,"Paola Guerra","Sureste"));
        arregloU.add(new Usuario(5,"Teresa Martínez","Centro"));
    }

    //Lectura lista Usuarios
    public List<Usuario> getUsuarios(){return arregloU;}

    //Consulta por Usuario
    public Usuario getUsuario(long id_User) throws IndexOutOfBoundsException{
        if(getIdUser(id_User) >= 0){
            return arregloU.get(getIdUser(id_User));
        }
        return null;
    }

    public int getIdUser(long id_User) throws IndexOutOfBoundsException{
        int i=0;
        while(i<arregloU.size()){
            if(arregloU.get(i).getUsuarioId() == id_User){
                return(i);
            }
            i++;
        }
        return -1;
    }

    //Alta Usuario
    public Usuario agregarUsuario (Usuario nuevoUsuario){
        arregloU.add(nuevoUsuario);
        return nuevoUsuario;
    }

    //Baja Usuario
    public void eliminarUsuario(int id_user) throws IndexOutOfBoundsException {
        int posicion = arregloU.indexOf(arregloU.get(id_user));
        arregloU.remove(posicion);
    }

    //Cambio Usuario
    public Usuario actualizarUsuario(int id_user, Usuario nuevoUsuario) throws IndexOutOfBoundsException{
        arregloU.set(id_user,nuevoUsuario);
        return arregloU.get(id_user);
    }

    //cambio area
    public Usuario cambiarArea(int id_user, Usuario nuevaArea) throws IndexOutOfBoundsException{
        Usuario user = getUsuario(id_user);
        ///arregloU.set(id_user,(user.getNombreUS(),nuevaArea.getArea()));
        arregloU.set(id_user,nuevaArea);
        return arregloU.get(id_user);
    }


}
