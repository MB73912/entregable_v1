package entregable.api.cons.rest.servicio;

import entregable.api.cons.rest.modelo.Producto;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ServicioProducto {
    private List<Producto> listado = new ArrayList<Producto>();

    public ServicioProducto(){
        listado.add(new Producto(1,"producto 1",100.10));
        listado.add(new Producto(2,"producto 2",200.20));
        listado.add(new Producto(3,"producto 3",300.30));
        listado.add(new Producto(4,"producto 4",400.40));
        List<Integer> IdUsuarios = new ArrayList<>();
        IdUsuarios.add(1);
        IdUsuarios.add(3);
        listado.get(2).setIdUsuarios(IdUsuarios);
    }

    //Consulta de Lista
    public List<Producto> getProductos() {return listado;}

    //Consulta por producto
    public Producto getProducto(long indice) throws IndexOutOfBoundsException{
        if(getIndice(indice)>=0){
            return listado.get(getIndice(indice));
        }
        return null;
    }

    public int getIndice(long indice) throws IndexOutOfBoundsException{
        int i=0;
        while(i<listado.size()){
            if(listado.get(i).getId() == indice){
                return(i);
            }
            i++;
        }
        return -1;
    }

    //Alta de producto
    public Producto agregarProducto(Producto nuevoProd){
        listado.add(nuevoProd);
        return nuevoProd;
    }

    //Actualizar nombre del producto y precio
    public Producto actualizarProducto(int indice, Producto nuevoProd) throws IndexOutOfBoundsException{
        listado.set(indice, nuevoProd);
        return listado.get(indice);
    }

    //Elimina producto del listado
    public void eliminarProd(int indice) throws IndexOutOfBoundsException{
        int posicion = listado.indexOf(listado.get(indice));
        listado.remove(posicion);
    }

}
