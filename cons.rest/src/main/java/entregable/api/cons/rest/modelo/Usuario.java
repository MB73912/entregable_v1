package entregable.api.cons.rest.modelo;

public class Usuario {
    long usuarioId;
    String nombreUS;
    String area;

    public Usuario() {}

    public Usuario(long usuarioId, String nombreUS, String area){
        this.usuarioId = usuarioId;
        this.nombreUS = nombreUS;
        this.area = area;
    }

    public long getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(long usuarioId) {
        this.usuarioId = usuarioId;
    }

    public String getNombreUS() {
        return nombreUS;
    }

    public void setNombreUS(String nombreUS) {
        this.nombreUS = nombreUS;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

}
