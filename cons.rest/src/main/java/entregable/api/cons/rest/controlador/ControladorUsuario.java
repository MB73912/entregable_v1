package entregable.api.cons.rest.controlador;


import entregable.api.cons.rest.modelo.Usuario;
import entregable.api.cons.rest.servicio.ServicioUsuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/apirest/v1")
public class ControladorUsuario {

    @Autowired
    private ServicioUsuario servicioUsuario;

    @GetMapping("/usuarios")
    public List<Usuario> getUsuarios(){return servicioUsuario.getUsuarios();}

    @GetMapping("/usuarios/{id}")
    public ResponseEntity getUsuarioId(@PathVariable int id){
        Usuario user = servicioUsuario.getUsuario(id);
        if(user == null){
            return new ResponseEntity<>("Usuario no encontrado.", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(user);
    }

    @PostMapping("/usuarios")
    public ResponseEntity<String> agregarUsuario(@RequestBody Usuario usuario){
        servicioUsuario.agregarUsuario(usuario);
        return new ResponseEntity<>("Alta de usuario correcta.",HttpStatus.CREATED);
    }

    @DeleteMapping("/usuarios/{id}")
    public ResponseEntity eliminarUsuario(@PathVariable Integer id){
        Usuario user = servicioUsuario.getUsuario(id);
        if(user == null){
            return new ResponseEntity("El usuario no existe.",HttpStatus.NOT_FOUND);
        }
        servicioUsuario.eliminarUsuario(id-1);
        return new ResponseEntity("Usuario dado de baja.",HttpStatus.OK);
    }

    @PutMapping("/usuarios/{id}")
    public ResponseEntity<String> actualizarUsuario(@PathVariable int id, @RequestBody Usuario cambioUs){
        Usuario user = servicioUsuario.getUsuario(id);
        if(user == null){
            return new ResponseEntity<>("Usuario no encontrado.",HttpStatus.NOT_FOUND);
        }
        servicioUsuario.actualizarUsuario(id-1,cambioUs);
        return new ResponseEntity<>("Usuario actualizado correctamente.", HttpStatus.OK);
    }

    @PatchMapping("/usuarios/{id}")
    public ResponseEntity actualizarAreaUser(@PathVariable int id, @RequestBody Usuario cambioUS) {
        Usuario user = servicioUsuario.getUsuario(id);
        if (user == null) {
            return new ResponseEntity<>("Usuario no existe.", HttpStatus.NOT_FOUND);
        }
        user.setArea(cambioUS.getArea());
        servicioUsuario.actualizarUsuario(id - 1, user);
        return new ResponseEntity(user, HttpStatus.OK);
    }

}
