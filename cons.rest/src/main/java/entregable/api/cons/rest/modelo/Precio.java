package entregable.api.cons.rest.modelo;

public class Precio {
    private long id;
    private double precio;

    public Precio(){}
    public Precio(double precio){this.precio = precio;}

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }
}
