package entregable.api.cons.rest.modelo;

import java.util.List;
import java.util.Objects;

public class Producto {
    private long id;
    private String descripcion;
    private double precio;
    private List<Integer> idUsuarios;

    public Producto(){
    }

    public Producto (long id, String descripcion,double precio){
        this.id =id;
        this.descripcion = descripcion;
        this.precio = precio;
    }

    public Producto (long id, String descripcion, double precio, List<Integer> idUsuarios){
        this.id =id;
        this.descripcion = descripcion;
        this.precio = precio;
        this.idUsuarios = idUsuarios;
    }
    public long getId (){return id;}
    public void setId (long id){this.id = id;}

    public List<Integer> getIdUsuarios(){return this.idUsuarios;}
    public void setIdUsuarios(List<Integer> idUsuarios){this.idUsuarios = idUsuarios;}

    public String getDescripcion(){return descripcion;}
    public void setDescripcion(String descripcion){this.descripcion = descripcion;}

    public double getPrecio(){return precio;}
    public void setPrecio(double precio){this.precio = precio;}


}
