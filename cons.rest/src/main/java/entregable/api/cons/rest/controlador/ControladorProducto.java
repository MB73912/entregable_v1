package entregable.api.cons.rest.controlador;

import entregable.api.cons.rest.modelo.Precio;
import entregable.api.cons.rest.modelo.Producto;
import entregable.api.cons.rest.servicio.ServicioProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/apirest/v1")

public class ControladorProducto {
    @Autowired
    private ServicioProducto servicioProducto;

    @GetMapping("")
    public String root(){return "Bienvenido!!!!!";}

    @GetMapping("/productos")
    public List<Producto> getProductos() {return servicioProducto.getProductos();}

    @GetMapping("/productos/{id}")
    public ResponseEntity getProductoId(@PathVariable int id){
        Producto prod = servicioProducto.getProducto(id);
        if (prod == null){
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(prod);
    }

    @PostMapping("/productos")
    public ResponseEntity<String> agregarProducto(@RequestBody Producto producto){
        servicioProducto.agregarProducto(producto);
        return new ResponseEntity<>("Alta de Producto correcta.",HttpStatus.CREATED);
    }

    @PutMapping("/productos/{id}")
    public ResponseEntity<String> actualizarProducto(@PathVariable int id, @RequestBody Producto cambioProd){
        Producto prod = servicioProducto.getProducto(id);
        if (prod == null){
            return new ResponseEntity<>("Producto no encontrado.",HttpStatus.NOT_FOUND);
        }
        servicioProducto.actualizarProducto(id-1,cambioProd);
        return new ResponseEntity<>("Producto actualizado correctamente.",HttpStatus.OK);
    }

    @DeleteMapping("/productos/{id}")
    public ResponseEntity borrarProducto(@PathVariable Integer id){
        Producto prod = servicioProducto.getProducto(id);
        if (prod == null){
            return new ResponseEntity("Produdcto no encontrado.",HttpStatus.NOT_FOUND);
        }
        servicioProducto.eliminarProd(id-1);
        return new ResponseEntity("Producto eliminado exitosamente.",HttpStatus.OK);
    }

    @PatchMapping("/productos/{id}")
    public ResponseEntity patchPrecioProducto(@RequestBody Precio precio, @PathVariable int id){
        Producto prod = servicioProducto.getProducto(id);
        if (prod == null){
            return new ResponseEntity("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        prod.setPrecio(precio.getPrecio());
        servicioProducto.actualizarProducto(id-1,prod);
        return new ResponseEntity(prod,HttpStatus.OK);
    }

    @GetMapping("/productos/{id}/usuarios")
    public ResponseEntity getUsuarioXProd(@PathVariable int id){
        Producto prod = servicioProducto.getProducto(id);
        if (prod == null){
            return new ResponseEntity("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        if (prod.getIdUsuarios()!=null){
            return ResponseEntity.ok(prod.getIdUsuarios());
        }
        else
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
